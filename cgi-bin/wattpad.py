#!/bin/python3
import requests, bs4, json, re, os, pypandoc, sys
from md2gemini import md2gemini
from random import randint

# Provide path to pandoc
os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')

# Get user input
query = os.environ["QUERY_STRING"]
if not query:
    print("10 Enter your URL or ID: ")
    sys.exit()
elif query == "random":
    query = str(randint(10000000, 99999999))

# Getting id from url
try:
    search_id = re.compile(r'\d{8,}')
    id = search_id.search(query).group()
except:
    print("59 Incorrect URL or ID")

# Extract the info from the book
try:
    info = requests.get("https://www.wattpad.com/apiv2/info?id=" + id, headers={'User-Agent': 'Mozilla/5.0'}).json()
    title = info['title']
    desc = info['description']
    chapters = info['group']
    name = info['url']
    author = info['author']
    date = info['date']
    cover_url = info['cover']
except:
    print(f"51 Story not found with id '{id}'")

# For each chapters, get the text and add a title
full_html = ""
for i in range(len(chapters)):
    chapter = requests.get("https://www.wattpad.com/apiv2/storytext?id=" + str(chapters[i]['ID']), headers={'User-Agent': 'Mozilla/5.0'})
    soup = bs4.BeautifulSoup(chapter.text, 'html.parser')
    full_html += f"<h2>{chapters[i]['TITLE']}</h2>"
    full_html += soup.prettify()

# Convert html to md and md to gmi. Then return it in gmi format
full_md = pypandoc.convert_text(full_html, "md", "html")
full_gmi = md2gemini(full_md, links="paragraph")
print("20 text/gemini")
print(f"Published in {date} by {author}\n")
print(f"=> https://wattpad.com/{id} This story on Wattpad.")
print(f"=> https://wattpad.com/{author} Author's wattpad profile.")
print(f"=> {cover_url} Cover image")
print(full_gmi)
