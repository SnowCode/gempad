# GemPad

```
8""""8              8""""8
8    " eeee eeeeeee 8    8 eeeee eeeee
8e     8    8  8  8 8eeee8 8   8 8   8
88  ee 8eee 8e 8  8 88     8eee8 8e  8
88   8 88   88 8  8 88     88  8 88  8
88eee8 88ee 88 8  8 88     88  8 88ee8
```

Welcome to GemPad, a Wattpad mirror for Gemini. Currently experimental.

## Public demo
See the demo [here](gemini://snowcode.ovh/gempad) on gemini or [here](https://portal.snowcode.ovh/x/snowcode.ovh/gempad) on web proxy.

## Features
* Reading full Wattpad stories from URL or ID
* Read random stories (very experimental) 

### Roadmap
* Search queries
* Download stories
* Support other types of IDs 
* Giving more information about the stories like description, tags, etc.

## Building
### Dependencies
* Python3
* Pip3

### Steps
1. Download the source code

```bash
git clone https://codeberg.org/SnowCode/gempad
cd gempad/
```

2. Install the dependencies

```bash
pip install -r requirements.txt
```

3. Install jetforce (or skip this step if you already have a gemini server that supports cgi scripts)

```bash
pip install jetforce
```

4. Setup the capsule! (replace the command by yours if you use something else than jetforce)

```bash
# localhost
jetforce --dir .

# domain.tld
jetforce --hostname domain.tld --dir .
```

## About me
I am just a random person making random scripts on the internet

* Gemini: [gemini://snowcode.ovh](gemini://snowcode.ovh), [gemini://tilde.team/~snowcode](gemini://tilde.team/~snowcode), [web proxy](https://portal.snowcode.ovh/~snowcode)
* IRC: `SnowCode` on irc.tilde.chat and irc.libera.chat. Also lurking in [#snowcodeblog](https://tilde.chat/kiwi/#snowcodeblog) on irc.tilde.chat.

## License
This project is under the GNU-GPLv3 license. [More info here.](LICENSE)
